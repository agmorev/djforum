from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import UserManager
from django.utils.translation import ugettext_lazy as _


class User(AbstractBaseUser, PermissionsMixin):
    """Custom User model"""
    email       = models.EmailField(_('Электронная почта'), max_length=50, unique=True,
                    error_messages={'unique':_('Пользователь с таким адресом электронной почты уже существует')})
    last_name   = models.CharField(_('Фамилия'), max_length=50)
    first_name  = models.CharField(_('Имя'), max_length=50)
    middle_name = models.CharField(_('Отчество'), max_length=50, null=True, blank=True)
    photo       = models.ImageField(_('Изображение'), upload_to='accounts/images/', null=True, blank=True)
    location    = models.CharField(_('Место жительства'), max_length=30, blank=True)
    birth_date  = models.DateField(_('Дата рождения'), null=True, blank=True)
    is_staff    = models.BooleanField(_('Права менеджера'), default=False)
    is_active   = models.BooleanField(_('Активность пользователя'), default=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['last_name', 'first_name']

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')

    def get_short_name(self):
        return self.email

    def get_full_name(self):
        return " ".join([self.first_name, self.last_name])

    def __str__(self):
        return self.get_full_name()
