from .forms import SignupForm, ProfileForm
from django.views.generic.edit import CreateView, UpdateView
from .models import User
from django.urls import reverse_lazy


class SignupView(CreateView):
    """Logic for working with signup form and user model"""
    form_class = SignupForm
    template_name = 'registration/signup.html'
    success_url = reverse_lazy('login')


class ProfileView(UpdateView):
    """Logic for working with profile form and user model"""
    form_class = ProfileForm
    queryset = User.objects.all()
    template_name = 'accounts/profile.html'
    success_url = reverse_lazy('home')

    def get_object(self, queryset=None):
        return self.request.user