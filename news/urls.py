# news/urls.py

from django.urls import path
from . import views


urlpatterns = [
    # News patterns
    path('', views.scraped_data, name='news_list'),
]
