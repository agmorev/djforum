# news/views.py

from django.shortcuts import render
import requests
from bs4 import BeautifulSoup


def scraped_data(request):
    # session = requests.Session()
    # session.headers = {"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}
    url = "http://sfs.gov.ua/media-tsentr/novini"
    content = requests.get(url).content
    soup = BeautifulSoup(content, "html.parser")
    
    posts = soup.find_all('div', {'class':'news__title'})
    news = []
    for post in posts:
        published = post.find('span',{}).text
        title = post.find('a',{}).text
        link = "http://sfs.gov.ua" + post.find('a',{})['href']
        news.append([published, title, link])
    return render(request, "news/news-list.html", {'news': news })